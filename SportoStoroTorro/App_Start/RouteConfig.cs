﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SportoStoroTorro
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(null, "", new { controller = "Product", action = "List", category = (string)null, page = 1 });

            routes.MapRoute(null,
                 "Strona{page}",
                new { controller = "Product", action = "List", category = (string)null, page = 1 },
                new { page = @"\d+" }); //regex \d+ oznacza jedna lub wiecej liczbę całkowitą
            //przez co nie dziala np. /Product/apple a działa /Product/23

            routes.MapRoute(null, "{category}",
                new { controller = "Product", action = "List", page = 1 });

            routes.MapRoute(null, "{category}/Strona{page}",
                new { controller = "Product", action = "List" }, new { page = @"\d+" });

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Product", action = "List", id = UrlParameter.Optional }
            //);
            routes.MapRoute(null, "{controller}/{action}");
        }
    }
}
