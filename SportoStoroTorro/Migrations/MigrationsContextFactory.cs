﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using DataLogic.BusinessObjects.Implementation;
using System.Configuration;
namespace SportoStoroTorro.Migrations
{
    public class MigrationsContextFactory : IDbContextFactory<StoreContext>
    {
        private string ConnectionString;
        public MigrationsContextFactory(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public MigrationsContextFactory()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        }
        public StoreContext Create()
        {
            return new StoreContext(this.ConnectionString);
        }
    }
}