﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SportoStoroTorro.Startup))]
namespace SportoStoroTorro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
