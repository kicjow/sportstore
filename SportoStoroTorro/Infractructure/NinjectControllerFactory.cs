﻿using DataAccess.Repositories.Implementation;
using DataLogic.BusinessObjects.Contract;
using DataLogic.BusinessObjects.Implementation;
using DataLogic.Repositories.Contract;
using System.Data.Entity.Infrastructure;
using Moq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLogic.Migrations;
using DataLogic.Infractructure.Contract;
using DataLogic.Infractructure.Implementation;

namespace SportoStoroTorro.Infractructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            //Mock<IProductRepository> mock = new Mock<IProductRepository>();
            //mock.Setup(m => m.Products).Returns(new List<Product>
            //{
            //    new Product {Name = "piłka", Price=25},
            //    new Product {Name = "piłka1", Price=251},
            //    new Product {Name = "piłka2", Price=252},
            //    new Product {Name = "piłka3", Price=253},
            //}.AsQueryable());
            //ninjectKernel.Bind<IProductRepository>().ToConstant(mock.Object);

            ninjectKernel.Bind<IProductRepository>().To<ProductRepository>().WithConstructorArgument("connectionString", connectionString);
           
            ninjectKernel.Bind<IDbContextFactory<StoreContext>>().To<MigrationsContextFactory>().WithConstructorArgument("connectionString", connectionString);
            ninjectKernel.Bind<IProduct>().To<Product>();

            EmailSettings emailSetings = new EmailSettings()
            {
                writeasFile = bool.Parse(ConfigurationManager.AppSettings["Email.WriteAsFile"] ?? "false")
            };

            ninjectKernel.Bind<IOrderProcessor>().To<EmailOrderProcessor>().WithConstructorArgument("settings", emailSetings);
            ninjectKernel.Bind<IAuthProvider>().To<FormsAuthProvider>();

            
        }


    }
}