﻿using DataLogic.BusinessObjects.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SportoStoroTorro.Helpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo paginginfo, Func<int, string> pageUrl)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= paginginfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                var uri = pageUrl(i);
                tag.MergeAttribute("href", uri);
                tag.InnerHtml = i.ToString();
                if (i== paginginfo.CurrentPage)
                {
                    tag.AddCssClass("selected");
                }
                sb.Append(tag.ToString());
            }

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}