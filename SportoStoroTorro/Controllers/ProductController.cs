﻿using DataLogic.BusinessObjects.Contract;
using DataLogic.BusinessObjects.Implementation;
using DataLogic.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportoStoroTorro.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        public int PageSize = 4;
        public ProductController(IProductRepository prodRep)
        {
            this.repository = prodRep;
        }
        //
        // GET: /Product/
        public ActionResult Index()
        {
            return View();
        }

        public FileContentResult GetImage(int productId)
        {
            IProduct prod = repository.Products.FirstOrDefault(p => p.ProductID == productId);
            if (prod != null)
            {
                return File(prod.ImageData, prod.ImageMimeType);
            }
            else
            {
                return null;
            }
        }

        public ViewResult List(string category,int page = 1)
        {
            ProductListViewModel viewModel = new ProductListViewModel()
            {
                Products = repository.Products.OrderBy(p=> p.ProductID)
                .Where(p => category == null || p.Category == category)
                .Skip((page-1)*PageSize)
                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ? repository.Products.Count() : repository.Products.Where(e=> e.Category == category).Count()
                },
                CurrentCategory = category
            };

            return View(viewModel);
          
        }
    }
}