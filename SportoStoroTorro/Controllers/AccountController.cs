﻿using DataLogic.BusinessObjects.Implementation;
using DataLogic.Infractructure.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportoStoroTorro.Controllers
{
    public class AccountController : Controller
    {
        IAuthProvider authProvider;
        // GET: Account
        public AccountController(IAuthProvider auth)
        {
            this.authProvider = auth;
        }

        public ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (authProvider.Authenticate(model.UserName, model.Password))
                {
                    return Redirect(returnUrl ?? Url.Action("Index", "Admin"));
                }
                else
                {
                    ModelState.AddModelError("", "zle haslo lub nazwa usera");
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}