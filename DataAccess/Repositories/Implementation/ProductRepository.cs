﻿using DataLogic.BusinessObjects.Implementation;
using DataLogic.Repositories.Contract;
using DataLogic.BusinessObjects.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories.Implementation
{
    public class ProductRepository : IProductRepository
    {
        private StoreContext context;

        public ProductRepository(string connectionString)
        {
            this.context = new StoreContext(connectionString);
        }

        IQueryable<IProduct> IProductRepository.Products
        {
            get { return context.Products; }
        }


        public void SaveProduct(IProduct product)
        {
            if (product.ProductID == 0)
            {
                context.Products.Add(product as Product);
            }
            else
            {
                IProduct dbEntry = context.Products.Find(product.ProductID);
                if (dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.Description = product.Description;
                    dbEntry.Price = product.Price;
                    dbEntry.Category = product.Category;
                    dbEntry.ImageData = product.ImageData;
                    dbEntry.ImageMimeType = product.ImageMimeType;
                }
            }
            context.SaveChanges();
            
        }


        public IProduct DeleteProduct(int productId)
        {
            IProduct product = context.Products.Find(productId);
            if (product != null)
            {
                context.Products.Remove(product as Product);
                context.SaveChanges();
            }
            return product;

        }
    }
}
