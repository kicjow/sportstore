﻿using DataLogic.BusinessObjects.Contract;
using DataLogic.BusinessObjects.Implementation;
using DataLogic.Repositories.Contract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportoStoroTorro.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.WebPages.Html;
using SportoStoroTorro.Helpers;
using DataLogic.Infractructure.Contract;

namespace SportoStoroTorro.Tests.Tests
{
    [TestClass]
    public class OveralTests
    {
        [TestMethod]
        public void Can_Paginate()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]{
                new Product {ProductID=1, Name="P1"},
                 new Product {ProductID=2, Name="P2"},
                  new Product {ProductID=3, Name="P3"},
                   new Product {ProductID=4, Name="P4"},
                    new Product {ProductID=5, Name="P5"}
            }.AsQueryable());

            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;

            ProductListViewModel result = (ProductListViewModel)controller.List(null, 2).Model;

            IProduct[] prodArray = result.Products.ToArray();

            Assert.IsTrue(prodArray.Length == 2);
            Assert.AreEqual(prodArray[0].Name, "P4", true);
            Assert.AreEqual(prodArray[1].Name, "P5", true);
        }

        [TestMethod]
        public void Can_Generate_Page_links()
        {
            System.Web.Mvc.HtmlHelper myhelper = null;
            PagingInfo pagingInfo = new PagingInfo()
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            Func<int, string> PageUrlDelegate = i => "Strona" + i;
            MvcHtmlString result = myhelper.PageLinks(pagingInfo, PageUrlDelegate);

            Assert.AreEqual(result.ToString(), @"<a href=""Strona1"">1</a>" +
                @"<a class=""selected"" href=""Strona2"">2</a>" +
                @"<a href=""Strona3"">3</a>");
        }
        [TestMethod]
        public void Can_Filter_Products()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
               new Product {ProductID=1, Name="P1", Category="cat1"},
                 new Product {ProductID=2, Name="P2", Category="cat1"},
                  new Product {ProductID=3, Name="P3", Category="cat2"},
                   new Product {ProductID=4, Name="P4", Category="cat2"},
                    new Product {ProductID=5, Name="P5", Category="cat3"}
            }.AsQueryable);

            ProductController controller = new ProductController(mock.Object);

            controller.PageSize = 3;

            IProduct[] result = ((ProductListViewModel)controller.List("cat2", 1).Model).Products.ToArray();

            Assert.AreEqual(result.Length, 2);
            Assert.IsTrue(result[0].Name == "P3" && result[0].Category == "cat2");
            Assert.IsTrue(result[1].Name == "P4" && result[0].Category == "cat2");
        }

        [TestMethod]
        public void Can_create_Categories()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(
                new Product[] {
               new Product {ProductID=1, Name="P1", Category="cat1"},
                 new Product {ProductID=2, Name="P2", Category="cat1"},
                  new Product {ProductID=3, Name="P3", Category="cat2"},
                   new Product {ProductID=4, Name="P4", Category="cat2"},
                    new Product {ProductID=5, Name="P5", Category="cat3"}
                }.AsQueryable()
                );

            NavController controller = new NavController(mock.Object);

            string[] result = ((IEnumerable<string>)controller.Menu().Model).ToArray();

            Assert.AreEqual(result.Length, 3);
            Assert.AreEqual(result[0], "cat1");
            Assert.AreEqual(result[1], "cat2");
            Assert.AreEqual(result[2], "cat3");
        }

        [TestMethod]
        public void Indicates_selected_Category()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(
                new Product[] {
               new Product {ProductID=1, Name="P1", Category="cat1"},
                 new Product {ProductID=2, Name="P2", Category="cat1"},
                  new Product {ProductID=3, Name="P3", Category="cat2"},
                   new Product {ProductID=4, Name="P4", Category="cat2"},
                    new Product {ProductID=5, Name="P5", Category="cat3"}
                }.AsQueryable()
                );

            NavController controller = new NavController(mock.Object);

            string categoryToSelect = "cat3";

            string result = controller.Menu(categoryToSelect).ViewBag.SelectedCategory;
            Assert.AreEqual(categoryToSelect, result);
        }

        [TestMethod]
        public void Generate_category_specific_productCount()
        {

            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(
                new Product[] {
               new Product {ProductID=1, Name="P1", Category="cat1"},
                 new Product {ProductID=2, Name="P2", Category="cat1"},
                  new Product {ProductID=3, Name="P3", Category="cat2"},
                   new Product {ProductID=4, Name="P4", Category="cat2"},
                    new Product {ProductID=5, Name="P5", Category="cat3"}
                }.AsQueryable()
                );

            var target = new ProductController(mock.Object);

            int res1 = ((ProductListViewModel)target.List("cat1").Model).PagingInfo.TotalItems;
            int res2 = ((ProductListViewModel)target.List("cat2").Model).PagingInfo.TotalItems;
            int res3 = ((ProductListViewModel)target.List("cat3").Model).PagingInfo.TotalItems;
            int resALL = ((ProductListViewModel)target.List(null).Model).PagingInfo.TotalItems;

            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resALL, 5);
        }

        [TestMethod]
        public void CanLoginWithValidCredentials()
        {
            Mock<IAuthProvider> mock = new Mock<IAuthProvider>();
            mock.Setup(m => m.Authenticate("admin", "asd")).Returns(true);

            LoginViewModel model = new LoginViewModel()
            {
                UserName = "admin",
                Password = "asd"
            };

            AccountController target = new AccountController(mock.Object);

            ActionResult result = target.Login(model, "/MyUrl");
            Assert.IsInstanceOfType(result, typeof(RedirectResult));
            Assert.AreEqual("/MyUrl", ((RedirectResult)result).Url);
        }

        [TestMethod]
        public void CannotLoginWithBadCredentials()
        {
            Mock<IAuthProvider> mock = new Mock<IAuthProvider>();
            mock.Setup(m => m.Authenticate("admin2", "asd2")).Returns(true);

            LoginViewModel model = new LoginViewModel()
            {
                UserName = "admin2",
                Password = "asd2"
            };

            AccountController target = new AccountController(mock.Object);

            ActionResult result = target.Login(model, "/MyUrl");
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(((ViewResult)result).ViewData.ModelState.IsValid);
        }

    }
}
