﻿using DataLogic.BusinessObjects.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportoStoroTorro.Tests.Tests
{
    [TestClass]
    public class CartTests
    {
        [TestMethod]
        public void Can_Add_NewLine()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            Cart target = new Cart();
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            CartLine[] result = target.Lines.ToArray();

            Assert.AreEqual(result.Length, 2);
            Assert.AreEqual(result[0].Product, p1);
            Assert.AreEqual(result[1].Product, p2);
        }

        [TestMethod]
        public void CannAddQuantitytForExistingLines()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            Cart target = new Cart();
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            target.AddItem(p1, 10);


            CartLine[] result = target.Lines.OrderBy(x => x.Product.ProductID).ToArray();

            Assert.AreEqual(result.Length, 2);
            Assert.AreEqual(result[0].Quantity, 11);
            Assert.AreEqual(result[1].Quantity, 1);

        }
        [TestMethod]
        public void CanRemoveLine()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            Cart target = new Cart();
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            target.AddItem(p1, 10);

            target.RemoveLine(p1);

            Assert.AreEqual(target.Lines.Where(c => c.Product == p1).Count(), 0);
            Assert.AreEqual(target.Lines.Count(), 1);

        }

        [TestMethod]
        public void CAlculateCartTotal()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100 };
            Product p2 = new Product { ProductID = 2, Name = "P2", Price = 150 };

            Cart target = new Cart();
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);

            decimal result = target.ComputeTotalValue();
            Assert.AreEqual(result, 250);
        }

        [TestMethod]
        public void CanClearCOntent()
        {
            Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100 };
            Product p2 = new Product { ProductID = 2, Name = "P2", Price = 150 };

            Cart target = new Cart();
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            target.Clear();

            Assert.AreEqual(target.Lines.Count(), 0);
        }

    }
}
