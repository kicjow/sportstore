﻿using DataLogic.BusinessObjects.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogic.Repositories.Contract
{
    public interface IProductRepository
    {
        IQueryable<IProduct> Products { get; }
        void SaveProduct(IProduct product);
        IProduct DeleteProduct(int productId);
    }
}
