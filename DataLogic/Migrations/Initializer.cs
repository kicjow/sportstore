namespace DataLogic.Migrations
{
    using DataLogic.BusinessObjects.Implementation;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class Initializer : DropCreateDatabaseIfModelChanges<DataLogic.BusinessObjects.Implementation.StoreContext>
    {
      
        protected override void Seed(DataLogic.BusinessObjects.Implementation.StoreContext context)
        {
            //TODO: seed
            context.Products.AddOrUpdate(
                p => p.ProductID,
                new Product { ProductID = 1, Name = "P1", Category = "szachy", Description = "siusiak", Price = 1001 },
                  new Product { ProductID = 2, Name = "P2", Category = "szachy", Description = "dupcie", Price = 1002 },
                   new Product { ProductID = 3, Name = "P3", Category = "baseny", Description = "matjki", Price = 1003 },
                    new Product { ProductID = 4, Name = "P4", Category = "baseny", Description = "czepki", Price = 1004 },
                     new Product { ProductID = 5, Name = "P5", Category = "samochody", Description = "alufelgi", Price = 1005 }
                );

            context.SaveChanges();
        }
    }
}
