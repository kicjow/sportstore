﻿using DataLogic.Infractructure.Contract;
using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogic.Infractructure.Implementation
{
    public class FormsAuthProvider : IAuthProvider
    {
        public bool Authenticate(string username, string password)
        {
            bool result = Membership.ValidateUser(username, password);
            if (result)
            {
                FormsAuthentication.SetAuthCookie(username, false);
            }
            return result;
        }
    }
}
