﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogic.BusinessObjects.Implementation
{
    public class ShippingDetails
    {
        public string Name { get; set; }
        public string Line1 { get; set; }
        public string City { get; set; }
        public bool GiftWrap { get; set; }
    }
}
