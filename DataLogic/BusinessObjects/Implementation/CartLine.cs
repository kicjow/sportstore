﻿using DataLogic.BusinessObjects.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLogic.BusinessObjects.Implementation
{
    public class CartLine
    {
        public CartLine()
        {
            this.Quantity = 0;
        }
        public IProduct Product { get; set; }
        public int Quantity { get; set; }
    }
}
