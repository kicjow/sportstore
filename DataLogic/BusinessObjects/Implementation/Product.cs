﻿using DataLogic.BusinessObjects.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace DataLogic.BusinessObjects.Implementation
{
    public class Product : IProduct
    {
        [Key]
        [HiddenInput(DisplayValue = true)]
        public int ProductID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        [DataType(DataType.MultilineText)]
        public string Description
        {
            get;
            set;
        }

        public decimal Price
        {
            get;
            set;
        }

        public string Category
        {
            get;
            set;
        }


        public byte[] ImageData
        {
            get
            ;
            set
            ;
        }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType
        {
            get
           ;
            set
           ;
        }
    }
}
