﻿using DataLogic.BusinessObjects.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogic.BusinessObjects.Implementation
{
    public class ProductListViewModel
    {
        public IEnumerable<IProduct> Products{ get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
    }
}
