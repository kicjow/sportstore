﻿using DataLogic.BusinessObjects.Contract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogic.BusinessObjects.Implementation
{
    public class StoreContext : DbContext
    {
        public StoreContext(string connectionString)
            : base(connectionString)
        {
         
        }


      
        public DbSet<Product> Products { get; set; }
    }
}
