﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLogic.BusinessObjects.Contract
{
    public interface IProduct
    {
        int ProductID { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        decimal Price { get; set; }
        string Category { get; set; }

        public  byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
    }
}
